---
sidebarDepth: 2
---

# Locations

There are several common locations in System51.


## Technithia
3rd planet from the sun, Technithia is the hub of technology in System51. Much
of the planet's surface is covered in urban areas with carefully arranged
greenery on skyscraper roofs in order to keep the balance between the planet
and the people on it.

### The Company's Station
The Company is based on an orbital station around Technithia. This allows it to
remain internationally free from oversight and allows it's employees to enjoy
relative freedom from consequences.

The station includes:
* living quarters
* meeting rooms
* canteen and kitchen
* offices

## Xyron
2nd planet from the sun. The governmental system closely resembles ancient
fuedal Japan with war lords frequently taking control of companies on the planet
by force. Battles over honour are common.

Recently (within a generation) the assasination of the emperor caused a stir
among the planet's population. Although the culprit was never found, various
theories have arisen. Ask any Xyron citizen and they will have their own view
on the matter.

## Asteroid Belt

Far out of the solar system is an unusual asteroid belt. Made of igneous rock
and carved out with smooth edges and sharp points it is a point of great 
scientific interest.

Recently, ancient alien, *Eidochron*, tech has been discovered. Efforts to use the
technology have frequently resulted in disaster but the profit to be gained
fuels futher testing. Technologies built from Eidochron base include:
* Teleporters
* Super soldiers
* Desk ornaments