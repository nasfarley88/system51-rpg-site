# NPCs

As NPCs are introduced into the story, their backstories and known characteristics will be updated here.

## Leslie "Trunchball" Dyers

<npc-info-box name="trunchball"></npc-info-box>

Head of the players parent mercenary company, The Company. Built like a tank
(with a temper like one too) her management style is ... aggressive. Constantly after the approval of her parents, she hates her sister [Debbie](#debbie-dyers).

## Debbie Dyers
<npc-image src="/debbie_by_mebiusu-dclmm7z-shrunk.png" credit-link="https://www.deviantart.com/mebiusu/art/Debbie-761921279" credit-author="Benedict Farley"></npc-image>

<npc-info-box name="debbie"></npc-info-box>

A down and out bounty hunter/mercenary, Debbie is forced to work at HoneyShields.

Her sister is known to be [Trunchball](#leslie-trunchball-dyers).

She is rumoured to have distant ancestors from an ancient Earth villaged named Accounting.

## Kyoko Don'yokuna

<npc-image src="/kyoko_by_mebiusu-dcmrlc0-shrunk.png" credit-link="https://www.deviantart.com/mebiusu/art/Kyoko-763833024" credit-author="Benedict Farley"></npc-image>

Kyoko rose to power from the lowest parts of Xyron society. She is currently
head of the Xyron embassy on Technithia.

## Craig Zanderthip
<npc-image src="/craig_by_mebiusu-dcnqrc4-shrunk.png" credit-link="https://www.deviantart.com/mebiusu/art/Craig-765473764" credit-author="Benedict Farley"></npc-image>
<npc-info-box name="craig"></npc-info-box>

Proud owner of a prize-winning fennec fox, Craig Zanderthip is a wealthy
businessman who looks like he's about to sell you something. Usually, because he
is! Having sucessfully raised his website to the point where he can pay others
to manage the day-to-day stuff, he chooses to sell whatever's popular to whoever
is listening.

## Derek Dinath
One of the best pilots on offer, Derek is Deaf and proud. He's also hard-headed and difficult to work with. Sufficient to say, he'll pilot any ship as long as you pay him well enough, but you wouldn't invite him to your wedding.
